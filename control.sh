#!/bin/bash
export RUNNER_UID=`id -u`
export RUNNER_GID=`id -g`
export `cat ./env/*`

case $1 in
  "build")
      docker-compose down
      docker-compose build
    ;;
  "start")
      docker-compose stop
      docker-compose up -d
    ;;
  "stop")
      docker-compose stop
    ;;
  "uninstall")
      docker-compose stop
      docker-compose down
    ;;
esac
