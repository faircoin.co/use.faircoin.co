---
id: FCLN_<!-- enter short id max. 5 letters in uppercase -->
name: "<!-- enter local nodes name(city or region) -->"
description: "<!-- add some descriptions or helpful infos to the local node -->"
contact: "<!-- add contact address, email, telegram, etc. -->"
established: YYYY-MM-DD<!-- enter date when the local node was established -->
assembly_notes: "<!-- add link to your (recent) assembly notes -->"
icon: "<!-- attach image to this issue -->"
latitude: <!-- get coordinates here https://latlong.net -->
longitude: <!-- get coordinates here https://latlong.net -->
color: <!-- find names here https://htmlcolorcodes.com/color-names/ -->
type: Point
status: active <!-- status can be active, passive or inactive -->
---
### localnode data
<!-- change the data on top of this template -->

/label ~"add localnode"
