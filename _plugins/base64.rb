require 'base64'

module B64
  def b64decode(input)
    Base64.decode64(input)
  end
end

Liquid::Template.register_filter(B64) # register filter globally
