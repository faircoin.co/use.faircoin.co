# -*- coding: utf-8 -*-

import sys
import os
import json
import base64
import wget
from urllib.error import HTTPError

file = open('./usefaircoin.json','r')
s=file.read()
file.close()

J=json.loads(s)['response']

for j in J:
    tmp=''
    for k in j:

        if k == 'type':
            continue

        tmp+='\n'

        if( type(j[k]) is str ):
            tmp+= k + ': ' + j[k] + '\n'
        else:
            if( k != 'geometry' and k != 'properties' ):
                tmp+=k+':\n'

            for l in j[k]:
                if( l != 'categories' ):
                    if( type(j[k][l]) is str ):
                        if( k != 'geometry' ):
                            if( l == 'featured_img_src' or l == 'image' ):
                                fn=str( j['properties']['id'] ) + '.' + j[k][l].split('.')[-1]
                                url=j[k][l]
                                j[k][l] = 'images/'+ l + '/' + fn

                                if not os.path.exists(j[k][l]):
                                    try:
                                        wget.download( url , out=j[k][l] )
                                    except ValueError:
                                        j[k][l] = ''
                                        pass
                                    except HTTPError as err:
                                        if err.code == 404:
                                            j[k][l] = ''
                                            pass


                            if( l == 'email' or l == 'phone' or l == 'twitter' ):
                                tmp+=  l + ': "' + ( base64.b64encode( j[k][l].replace('\n','\n').encode('utf-8') ) ).decode('utf-8') + '"\n'
                            else:
                                tmp+=  l + ': ' + json.dumps( j[k][l].replace('\n','\n') ) + '\n'

                    elif( type(j[k][l]) is list ):
                        if( l == 'coordinates' ):
                            tmp+= 'latitude: ' + str( round( float( j[k][l][1] ), 6 ) ) + '\n'
                            tmp+= 'longitude: ' + str( round( float( j[k][l][0] ), 6 ) ) + '\n'
                        else:
                            tmp+=  l + ': [' + ','.join(j[k][l]) + ']\n'

    file = open('./_data/' + str( j['properties']['id'] ) + '.yml','w+')
    file.write( tmp )
    file.close()

    mon,yy=j['properties']['date'].split('/')
    mm={
     'Jan' : '01',
     'Feb' : '02',
     'Mar' : '03',
     'Apr' : '04',
     'May' : '05',
     'Jun' : '06',
     'Jul' : '07',
     'Aug' : '08',
     'Sep' : '09',
     'Oct' : '10',
     'Nov' : '11',
     'Dec' : '12'
    }

    tmp='---\nlayout: usefaircoin\n---'
    fn='./_posts/' + yy + '-'+mm[mon]+'-01-'+ str( j['properties']['id'] ) +'.md'
    if not os.path.exists(fn):
        file = open(fn,'w+')
        file.write( tmp )
        file.close()
